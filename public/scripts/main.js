let myImage = document.querySelector('img')

myImage.onclick = () => {
    let mySrc = myImage.getAttribute('src')
    console.log(mySrc)
    if (mySrc === 'images/firefox-icon.png') {
        myImage.setAttribute('src', 'images/google-icon.png')
    } else {
        myImage.setAttribute('src', 'images/firefox-icon.png')
    }
}

let myButton = document.querySelector('button')
let myHeading = document.querySelector('h1')

function setUserName () {
  let myName = prompt('Please enter your name.')
  localStorage.setItem('name', myName)
  myHeading.textContent = 'Mozilla is cool, ' + myName
}

if (!localStorage.getItem('name')) {
    setUserName()
} else {
    const storedName = localStorage.getItem('name')
    myHeading.textContent = 'Mozilla is cool, ' + storedName
}

myButton.onclick = () => {
    setUserName()
}